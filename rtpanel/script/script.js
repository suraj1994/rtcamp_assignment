$(document).ready(function(){

    $('html, body').animate({
        scrollTop: 0
      }, 900);

    //for right left navigation
    $('#right-button').click(function() {
	      event.preventDefault();
	      $('#content').animate({
	        scrollLeft: "+=300px"
	      }, "slow");
	   });
   
    $('#left-button').click(function() {
	      event.preventDefault();
	      $('#content').animate({
	        scrollLeft: "-=300px"
	      }, "slow");
	   });

    $('#right-button-partner').click(function() {
	      event.preventDefault();
	      $('#contentpartner').animate({
	        scrollLeft: "+=300px"
	      }, "slow");
	   });
   
    $('#left-button-partner').click(function() {
	      event.preventDefault();
	      $('#contentpartner').animate({
	        scrollLeft: "-=300px"
	      }, "slow");
	   });
	    

	//for time and date
	var month = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
	var day = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

	var newDate = new Date();

	newDate.setDate(newDate.getDate());

	$('#date').html(day[newDate.getDay()] + ", " + newDate.getDate() + " " + month[newDate.getMonth()]);

	//for hour
	setInterval( function() {
		var hours = new Date().getHours();
		$("#hr").html(( hours < 10 ? "0" : "" ) + hours);
	}, 1000);

	//for minute
	setInterval( function() {
		var minutes = new Date().getMinutes();
		$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
	},1000);

	//for second
	setInterval( function() {
		var seconds = new Date().getSeconds();
		$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
	},1000);
		
});